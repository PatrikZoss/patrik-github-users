import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {UserData} from '../../shared/types/user-data';
import {UsersRestService} from '../../shared/services/users-rest.service';
import {ActivatedRoute} from '@angular/router';
import {RepositoryData} from '../../shared/types/repository-data';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  user$: Observable<UserData> | undefined;

  followers$: Observable<UserData[]> | undefined;

  login: string | null | undefined;

  pageSizeFollowers: 5 | undefined;

  pageSizeRepositories: 5 | undefined;

  repositories$: Observable<RepositoryData[]> | undefined;

  constructor(private usersRestService: UsersRestService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .subscribe(urlParams => {
        this.login = urlParams.get('id');

        this.user$ = this.usersRestService.getUserDetail(this.login);

        this.followers$ = this.usersRestService.getUserFollowers(this.login, 0, 5);

        this.repositories$ = this.usersRestService.getUserRepositories(this.login, 0, 5);
      });
  }

  onPageChangeFollowers(event: any): void {
    if (this.login) {
      this.followers$ = this.usersRestService.getUserFollowers(this.login, event.pageIndex + 1, event.pageSize);
    }
  }

  onPageChangeRepositories(event: any): void {
    if (this.login) {
      this.repositories$ = this.usersRestService.getUserRepositories(this.login, event.pageIndex + 1, event.pageSize);
    }
  }
}
