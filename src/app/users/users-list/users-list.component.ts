import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersRestService} from '../../shared/services/users-rest.service';
import {UsersSearchResult} from '../../shared/types/users-search-result';
import {SelectItem} from '../../shared/types/select-item';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {

  pageSize = 10;

  usersSearchResult$: UsersSearchResult | undefined;

  searchQuery = 'Bratislava';

  totalCount: number | undefined;

  sortBy = 'repositories';

  direction = 'asc';

  page = 0;

  sortOptions: SelectItem[] = [
    {label: 'Repositories', value: 'repositories'},
    {label: 'Followers', value: 'followers'},
    {label: 'Created at', value: 'joined'}
  ];

  directionOptions: SelectItem[] = [
    {label: 'Ascending', value: 'asc'},
    {label: 'Descending', value: 'desc'}
  ];

  private usersSearchResult: Subscription | undefined;

  constructor(private usersRestService: UsersRestService) {
  }

  ngOnInit(): void {
    this.searchUsers(this.searchQuery, 0, 10, this.sortBy, this.direction);
  }

  onPageChange(event: any): void {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.searchUsers(this.searchQuery, this.page, this.pageSize, this.sortBy, this.direction);
  }

  searchUsers(location: string, page: number | undefined, pageSize: number | undefined, sortBy: string, orderBy: string): void {
    if (location) {
      this.usersSearchResult = this.usersRestService.searchByLocation(location, page, pageSize, sortBy, orderBy).subscribe(
          res => {
            this.usersSearchResult$ = res;
            this.totalCount = res.total_count;
          }
      );
    }
  }

  sortedSearchUsers(): void {
    this.searchUsers(this.searchQuery, this.page, this.pageSize, this.sortBy, this.direction);
  }

  searchUsersByLocation(): void {
    if (this.searchQuery?.length > 5) {
      this.searchUsers(this.searchQuery, this.page, this.pageSize, this.sortBy, this.direction);
    }
  }

  ngOnDestroy(): void {
    this.usersSearchResult?.unsubscribe();
  }
}
