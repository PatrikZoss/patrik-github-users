import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersListComponent} from './users-list/users-list.component';
import {RouterModule, Routes} from '@angular/router';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';

const routes: Routes = [
  {path: '', component: UsersListComponent},
  {path: ':id', component: UserDetailComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        MatPaginatorModule,
        FormsModule,
        MatSelectModule
    ],
  exports: [RouterModule],
  declarations: [UsersListComponent, UserDetailComponent],
})
export class UsersModule {
}
