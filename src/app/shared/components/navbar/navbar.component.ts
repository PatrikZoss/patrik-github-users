import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  gitHubUrl: string | undefined;

  isAuthenticated = false;

  userLogin: string | undefined;

  private userSubscription: Subscription | undefined;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.gitHubUrl = this.authService.getGitHubUrlForLogin();

    this.userSubscription = this.authService.user.subscribe(user => {
      this.isAuthenticated = !!user;
      if (user) {
        this.userLogin = user.login;
      }
    });
  }

  logoutUser(): void {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }


}
