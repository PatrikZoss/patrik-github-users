import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  accessToken: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    const gitAuthCode = this.route.snapshot.queryParamMap.get('code');

    if (gitAuthCode) {
      this.getAccesToken(gitAuthCode).subscribe(() => {
        this.router.navigate(['welcome']);
      });
    }

  }

  private getAccesToken(gitAuthCode: string): Observable<any> {
    const params = new HttpParams().set('code', gitAuthCode);
    this.accessToken = this.httpClient.get(environment.gatekeeperConfig.gatekeeper, {params})
      .pipe(
        map(response => {
          if (response) {
            this.accessToken = response;
            localStorage.setItem('access_token', this.accessToken.token);
            return {authenticated: true};
          } else {
            localStorage.removeItem('access_token');
            return {authenticated: false};
          }
        }));

    return this.accessToken;
  }


}
