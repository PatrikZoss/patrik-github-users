import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {UserData} from '../types/user-data';
import {RepositoryData} from '../types/repository-data';
import {UsersSearchResult} from '../types/users-search-result';

@Injectable({
  providedIn: 'root'
})
export class UsersRestService {

  constructor(private httpClient: HttpClient) {
  }

  getUserDetail(userId: string | null): Observable<UserData> {
    return this.httpClient.get<UserData>(`${environment.apiUrl}/users/` + userId);
  }

  getUserFollowers(login: string | null, page: number, pageSize: number | undefined): Observable<UserData[]> {
    const params = new HttpParams().set('page', String(page)).set('per_page', String(pageSize));
    return this.httpClient.get<UserData[]>(`${environment.apiUrl}/users/` + login +
      `/followers`, {params});
  }

  getUserRepositories(login: string | null, page: number, pageSize: number | undefined): Observable<RepositoryData[]> {
    const params = new HttpParams().set('page', String(page)).set('per_page', String(pageSize));
    return this.httpClient.get<RepositoryData[]>(`${environment.apiUrl}/users/` + login +
      `/repos`, {params});
  }

  searchByLocation(location: string, page: number | undefined, pageSize: number | undefined, sortBy: string, orderBy: string)
    : Observable<UsersSearchResult> {
    const params = new HttpParams().set('page', String(page)).set('per_page', String(pageSize)).set('sort', sortBy).set('order', orderBy);
    return this.httpClient.get<UsersSearchResult>(`${environment.apiUrl}/search/users?q=location:` + location, {params});
  }

  // TODO: it seems that it does not work !!! always return 404 error code
  getUserIssues(): Observable<any> {
    return this.httpClient.get<UserData>(`${environment.apiUrl}/user/issues`);
  }

}
