import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UserData} from '../types/user-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public gitHubUrl: string = 'https://github.com/login/oauth/authorize?client_id=' +
    environment.gatekeeperConfig.client_id + '&scope=users&redirect_uri=' +
    environment.gatekeeperConfig.redirect_uri;

  user = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient,
              private router: Router) {
  }

  public getProfile(): Observable<UserData> {
    return this.http.get(`${environment.apiUrl}/user`)
      .pipe(map(response => this.toLoggedUser(response)));
  }

  getGitHubUrlForLogin(): string {
    return this.gitHubUrl;
  }

  // tslint:disable-next-line:ban-types
  private toLoggedUser(response: Object): any {
    this.user.next(response);
  }

  logout(): void {
    this.user.next(null);
    localStorage.removeItem('access_token');
    this.router.navigate(['/welcome']);
  }
}
