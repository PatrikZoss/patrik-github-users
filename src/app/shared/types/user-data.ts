import {AbstractData} from './abstract-data';

export interface UserData extends AbstractData {
  avatar_url: string;
  events_url: string;
  followers_url: string;
  following_url: string;
  gists_url: string;
  gravatar_id: string;
  html_url: string;
  login: string;
  node_id: string;
  organizations_url: string;
  received_events_url: string;
  repos_url: string;
  site_admin: boolean;
  starred_url: string;
  subscriptions_url: string;
  type: string;
  followers: number;
  bio: string;
  blog: string;
  company: string;
  email: string;
  following: string;
  hireable: boolean;
  location: string;
  name: string;
  public_gists: number;
  public_repos: number;
  twitter_username: string;
}
