import {AbstractData} from './abstract-data';
import {UserData} from './user-data';

export interface UsersSearchResult extends AbstractData {
  incomplete_results: boolean;
  items: UserData[];
  total_count: number;
}
