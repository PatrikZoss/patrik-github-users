export interface AbstractData {
  id: number;
  url: string;
  created_at: Date;
  updated_at: Date;
}
