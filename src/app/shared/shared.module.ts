import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersRestService} from './services/users-rest.service';
import {NavbarComponent} from './components/navbar/navbar.component';
import {RouterModule} from '@angular/router';
import {AuthenticationComponent} from './components/authentication/authentication.component';


@NgModule({
  declarations: [NavbarComponent, AuthenticationComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    NavbarComponent
  ],
  providers: [
    UsersRestService
  ]
})
export class SharedModule {
}
