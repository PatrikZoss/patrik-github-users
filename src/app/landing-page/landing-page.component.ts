import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit, OnDestroy {

  title = 'garwan-project';

  gitHubUrl: string | undefined;

  private userProfileSubscription: Subscription | undefined;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.gitHubUrl = this.authService.getGitHubUrlForLogin();

    this.userProfileSubscription = this.authService.getProfile().subscribe();
  }

  ngOnDestroy(): void {
    this.userProfileSubscription?.unsubscribe();
  }
}
