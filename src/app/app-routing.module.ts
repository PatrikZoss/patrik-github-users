import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {AuthenticationComponent} from './shared/components/authentication/authentication.component';

const routes: Routes = [
  {path: '', redirectTo: 'welcome', pathMatch: 'full'},
  {path: 'auth', component: AuthenticationComponent},
  {path: 'welcome', component: LandingPageComponent},
  {path: 'users', loadChildren: () => import(`./users/users.module`).then(m => m.UsersModule)},
  {path: '**', component: LandingPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
