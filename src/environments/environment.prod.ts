export const environment = {
  production: true,
  apiUrl: 'https://api.github.com',
  gatekeeperConfig: {
    client_id: '41161c57230f38c36560',
    redirect_uri: 'https://patrik-github-users.herokuapp.com/auth',
    gatekeeper: '/auth.php'
  }
};
